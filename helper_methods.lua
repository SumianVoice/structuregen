-- helper functions
-- convert 3d position to a flat map index
function structuregen.to_flat_3d(p, sidelen)
    return p.x + (p.y * sidelen) + (p.z * sidelen^2) + 1
end
function structuregen.to_flat_2d(x, y, sidelen)
    return (x-1) + ((y-1) * sidelen) + 1
end

function structuregen.to_3d(i, sidelen)
    return -- not implemented
end
