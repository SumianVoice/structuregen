


structuregen.registered_structures = {}
structuregen.registered_structures_by_biome = {}
local structs = structuregen.registered_structures
local per_biome = structuregen.registered_structures

function structuregen.register_structure(def)
    --
    -- processing if needed
    --
    structs[def.name] = def
    -- put def into biome table
    for i, biome in ipairs(def.biomes) do
        if not per_biome[biome] then per_biome[biome] = {} end
        table.insert(per_biome[biome], def)
    end
end


structuregen.register_structure({
    biomes = {"grasslands"},
    name = "village", -- name of the key in the register
    -- figure out how to choose whether to place this or nothing
    place_threshold = 0.01, -- per chunk?
    filter_structures = {
        -- represents what is placed near
        [0x00000000000000000000000010011010] = {
            {
                sch = "some_schematic.mts",
                chance = 0.1, -- would have to be avg'd in the register
                on_generated = function(pos)
                    minetest.log("some_schematic.mts was placed")
                end,
            },
            {sch = "some_schematic.mts", chance=0.2}, -- can be simple if not using extra callbacks and stuff
        },
        ["catch"] = { -- if nothing else fit the filter
            {
                sch = "some_schematic.mts",
            }
        }
    },
    on_generated = function(minp, maxp)
        -- called instead of the default placement if present
    end and nil, -- disable
    on_structure_generated = function(pos, filter, structure_def) -- 
        -- e.g. structure_def == {sch = "some_schematic.mts", chance=0.2}
        -- e.g. filter == 0x00000000000000000000000010011010
    end,
})



