local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

structuregen = {}


dofile(mod_path .. "/helper_methods.lua")
dofile(mod_path .. "/pernoiseobj.lua")
dofile(mod_path .. "/register.lua")
dofile(mod_path .. "/generation.lua")
dofile(mod_path .. "/example.lua")
