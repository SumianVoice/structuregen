local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(minetest.get_current_modname())

local data = {}
local c_air = minetest.get_content_id("air")
minetest.register_on_generated(function(minp, maxp, seed)
	local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
	local area = VoxelArea:new{MinEdge = emin, MaxEdge = emax}

	local sidelen = maxp.x - minp.x + 1
	local permapdims3d = {x = sidelen, y = sidelen, z = sidelen}

	local height_2d = {}

	local ni = 1
	vm:get_data(data)
	for z = 1, maxp.z - minp.z + 1 do
	for y = 1, maxp.y - minp.y + 1 do
		local di = area:index(minp.x, y + minp.y, z + minp.z)
		for x = 1, maxp.x - minp.x + 1 do
			local rp = vector.new(x,y,z) -- relative position
			local flat2 = structuregen.to_flat_2d(rp.x, rp.z, sidelen)
            --
            --
			di = di + 1
			ni = ni + 1
		end
	end end
    vm:set_data(data)
	vm:calc_lighting()
	vm:write_to_map()
	vm:update_liquids()
end)

