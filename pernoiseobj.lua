



-- defines the perlin noise object which will store and handle flat map access
local perl = {}
perl.__index = perl

function perl:get_3d(pos)
    return self.perlin:get_3d(pos)
end

function perl:get_3d_map_flat(pos, sidelen, chunksize)
    self.map_flat_3d = self.map_flat_3d or {}
    if (not self.perlin_map_obj) or (self.sidelen ~= sidelen) then
        local s = math.ceil(chunksize/sidelen)
        self.perlin_map_obj = minetest.get_perlin_map(self.np, {x=s, y=s, z=s})
    end
    self.perlin:get_3d_map_flat(pos, self.map_flat_3d)
    return self.map_flat_3d
end

function perl:get_flat(i)
    return -- not implemented
end

function structuregen.new_perlinnoise(np)
    local ret = {
        np=np,
        perlin = PerlinNoise(np)
    }
    ret = setmetatable(ret, perl)
    return ret
end

local nm = structuregen.new_perlinnoise({
    offset = 50,
    scale = 50,
    spread = {x = 1000, y = 1000, z = 1000},
    seed = 7490,
    octaves = 5,
    persist = 0.5,
    lucanarity = 2.55,
})

minetest.log(nm:get_3d(vector.new(1, 343, 2)))
